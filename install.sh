#!/bin/bash
sudo apt-get update -y
sudo apt-get install python3 -y
sudo apt-get install python3-gpiozero - y
sudo systemctl enable pigpiod
sudo systemctl start pigpiod
echo 'MESSAGE:'
echo 'You have 2 things to do.'
echo '1. Set IP of Pi to 192.168.1.12 on router'
echo "2. Make sure to active 'Remote GPIO' with the 'sudo raspi-config' command"